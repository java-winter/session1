public class App {

	public static void main(String[] args) {
		System.out.println("Hello World");
		
		int x = 12;
		if (x ==12) {
			System.out.println("X is 12");
		}
		
		for (int j=0; j<x ; j++) {
			System.out.println(j);
		}
		
		System.out.println(getValue(x));
	}
	
	public static int getValue(int input) {
		return input * 2;
	}

}
